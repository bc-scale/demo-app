#!/usr/bin/env bash

# Increment a version string using Semantic Versioning (SemVer) terminology.

# Parse command line options.

die() {
  echo "usage: $(basename "$0") [-Mmpb] major.minor.patch-build (e.g. 1.2.3-4)" >&2
  exit 1
}

while getopts ":Mmpb" Option
do
  case $Option in
    M ) major=true;;
    m ) minor=true;;
    p ) patch=true;;
    b ) build=true;;
    * ) die ;;
  esac
done

shift $((OPTIND - 1))

full_version=$1
# shellcheck disable=SC2001
version_suffix=$(echo "$full_version" | sed 's/^[0-9.]*//')
if [ "x${version_suffix}" == "x" ]; then
    version="${full_version}"
else
    version="${full_version//${version_suffix}/}"
fi

# Build array from version string.

read -r -a a <<< "${version//./ }"

# If version string is missing or has the wrong number of members, show usage message.

if [ ${#a[@]} -ne 3 ]
then
  die
fi

# Increment version numbers as requested.

if [ -n "$major" ]
then
  ((a[0]++))
  a[1]=0
  a[2]=0
fi

if [ -n "$minor" ]
then
  ((a[1]++))
  a[2]=0
fi

if [ -n "$patch" ]
then
  ((a[2]++))
fi

if [ -n "$build" ]
then
  # e.g. version '1.2.3-rc42' -> build_number_prefix '-rc'
  build_number_prefix=$(echo "$version_suffix" | sed "s/[0-9]\\+\\.[0-9]\\+\\.[0-9]\\+//g" | sed "s/[0-9]\\+$//g")
  # e.g. version '1.2.3-rc42' -> build_number '42'
  build_number=$(echo "$version_suffix" | grep -Eo '[0-9]+$')

  regex='^[0-9]+$'
  if ! [[ $build_number =~ $regex ]] ; then
    echo "[ERROR] build_number not a number" >&2;
    die
  fi

  ((build_number++))
  version_suffix="${build_number_prefix}${build_number}"
fi

echo "${a[0]}.${a[1]}.${a[2]}${version_suffix}"
